<!DOCTYPE html>
<%@page import="in.co.sunrays.proj3.controller.ORSView"%>
<html lang="en">
<%@page import="in.co.sunrays.proj3.dto.UserDTO"%>
<head>
  <title>Header</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  
  
</head>
<body >
<form action="">
<table>

<nav class="navbar navbar-inverse" class="header">
  <div class="container-fluid">
    <div class="navbar-header">
     <img class="navbar-header" alt="" src="/pro3/image/customLogo.jpg" width="200px"
						height="45px">
						
    </div>
   
    <%
		UserDTO userBean=new UserDTO();/* = (UserDto) session.getAttribute("user"); */

		boolean userLoggedIn = userBean == null;

		String welcomeMsg = "Hi, ";

		if (userLoggedIn) {
			String role = (String) session.getAttribute("role");
			welcomeMsg += userBean.getFirstName() + " (" + role + ")";
		} else {
			welcomeMsg += "Guest";

		}
	%>
	 <ul class="nav navbar-nav"><li><font style="color: white"><h4>
					<%=welcomeMsg%></h4></font></li></ul>
	
    
    <ul class="nav navbar-nav"></ui>
     <li><a href="#" style="color: white"><span class="glyphicon glyphicon-home"></span>Home</a></li>
    
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">User<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Add user</a></li>
          <li><a href="#">User list</a></li>
        
        </ul>
      </li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Marksheet<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Add marksheet</a></li>
          <li><a href="#">Marksheet list</a></li>
          <li><a href="#">Marksheet merit list</a></li>
          <li><a href="#">Get marksheet</a></li>
        
        </ul>
      </li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" >Student<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Add student</a></li>
          <li><a href="#">Student list</a></li>
        
        </ul>
      </li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" >Faculty<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Add faculty</a></li>
          <li><a href="#">Faculty list</a></li>
        
        </ul>
      </li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Course<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Add Course</a></li>
          <li><a href="#">Course list</a></li>
        
        </ul>
      </li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" ">Subject<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Add subject</a></li>
          <li><a href="#">Subject list</a></li>
        
        </ul>
      </li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" >College<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Add college</a></li>
          <li><a href="#">College list</a></li>
        
        </ul>
      </li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" ">Role<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Add role</a></li>
          <li><a href="#">Role list</a></li>
        
        </ul>
      </li>
       <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Timetable<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Add timetable</a></li>
          <li><a href="#">Timetable list</a></li>
        
        </ul>
      </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#" style="color: white"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="<%=ORSView.LOGIN_CTL%>" style="color: white"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      
    </ul>
  </div>
</nav>
</div>
</table>
</form>
</body>
</html>
