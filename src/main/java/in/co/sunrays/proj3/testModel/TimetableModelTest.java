package in.co.sunrays.proj3.testModel;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.*;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;

import in.co.sunrays.proj3.dto.CollegeDTO;
import in.co.sunrays.proj3.dto.TimetableDTO;
import in.co.sunrays.proj3.exception.ApplicationException;
import in.co.sunrays.proj3.exception.DuplicateRecordException;
import in.co.sunrays.proj3.model.CourseModelHibImpl;
import in.co.sunrays.proj3.model.CourseModelInt;
import in.co.sunrays.proj3.model.TimetableModelHibImpl;
import in.co.sunrays.proj3.model.TimetableModelInt; 

public class TimetableModelTest {

	/** 
     * Model object to test 
     */ 
 
 public static TimetableModelInt model = new TimetableModelHibImpl(); 
 
   //public static UserModelInt model = new UserModelJDBCImpl(); 
 
    /** 
     * Main method to call test methods. 
     *  
     * @param args 
     * @throws ParseException 
     * @throws ApplicationException 
     */ 
    public static void main(String[] args) throws ParseException, ApplicationException{ 
      // testAdd(); 
        //testDelete(); 
       //testUpdate(); 
        //testFindByPK(); 
        // testFindByLogin(); 
       // testSearch(); 
        ///testList(); 
       // testGetRoles(); 
       //testAuthenticate(); 
         //testchangePassword(); 
       // testRegisterUser(); 
       // testforgetPassword(); 
       // testresetPassword();
    	//testFindByLogin();
    	testFindByLogin1();
   
    } 
    
    

    /**UserDTO dto = new UserDTO(); 
     * Tests get Search 
     */ 
    public static void testSearch() { 
 
        try { 
            TimetableDTO dto = new TimetableDTO(); 
            List list = new ArrayList(); 
            // dto.setFirstName("ranjit"); 
            // dto.setLastName("ch"); 
           // dto.setLogin("ankur44@gmail.com"); 
 
            list = model.search(dto, 0, 0); 
            if (list.size() < 0) { 
                System.out.println("Test Serach fail"); 
            } 
            Iterator it = list.iterator(); 
            while (it.hasNext()) { 
                dto = (TimetableDTO) it.next(); 
                System.out.println(dto.getCource_Name()); 
                System.out.println(dto.getExam_time()); 
                System.out.println(dto.getExam_Date()); 
                System.out.println(dto.getCreatedBy()); 
                System.out.println(dto.getModifiedBy()); 
                } 
 
        } catch (ApplicationException e) { 
            e.printStackTrace(); 
        } 
 
    } 
 
    /** 
     * Tests get List. 
     */ 
    public static void testList() { 
 
        try { 
            TimetableDTO dto = new TimetableDTO(); 
            List list = new ArrayList(); 
            list = model.list(0, 10); 
            if (list.size() < 0) { 
                System.out.println("Test list fail"); 
            } 
            Iterator it = list.iterator(); 
            while (it.hasNext()) { 
                dto = (TimetableDTO) it.next(); 
                System.out.println(dto.getCource_Name()); 
                System.out.println(dto.getExam_time()); 
                System.out.println(dto.getExam_Date()); 
                System.out.println(dto.getCreatedBy()); 
                System.out.println(dto.getModifiedBy()); 
                          } 
 
        } catch (ApplicationException e) { 
            e.printStackTrace(); 
        } 
    } 
 
    
    /** 
     * Tests find a User by Login. 
     */ 
    /*public static void testFindByLogin() { 
       TimetableDTO dto = new TimetableDTO(); 
        try { 
            dto = model.findBy("tit"); 
            if (dto == null) { 
                System.out.println("Test Find By PK fail"); 
            } 
            System.out.println(dto.getId()); 
            System.out.println(dto.getDuration()); 
            System.out.println(dto.getCourceId()); 
            System.out.println(dto.getCreatedBy()); 
            System.out.println(dto.getModifiedBy()); 
             } catch (ApplicationException e) { 
            e.printStackTrace(); 
        } 
    } 
 */
    /** 
     * Tests find a User by PK. 
     */ 
    public static void testFindByPK() { 
        try { 
            TimetableDTO dto = new TimetableDTO(); 
            long pk = 3L; 
            dto = model.findByPK(pk); 
            if (dto == null) { 
                System.out.println("Test Find By PK fail"); 
            } 
            System.out.println(dto.getCource_Name()); 
            System.out.println(dto.getExam_time()); 
            System.out.println(dto.getExam_Date()); 
            System.out.println(dto.getCreatedBy()); 
            System.out.println(dto.getModifiedBy()); 
               } catch (ApplicationException e) { 
            e.printStackTrace(); 
        } 
 
    } 
 
 
    /** 
     * Tests add a User 
     *  
     * @throws ParseException 
     */ 
   /* public static void testAdd() throws ParseException { 
    	System.out.println("add k aander");
 
        try { 
            TimetableDTO dto = new TimetableDTO(); 
         	System.out.println("dto par");
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy"); 
 
            // dto.setId(8L); 
            dto.setCource_Id(1);
            dto.setCource_Name("b.tech");
            dto.setSubject_Id(3);
            dto.setSubject_Name("elect");
            dto.setSemester("3");
            dto.setExam_time("10 am to 1 pm");
            dto.setExam_Date(sdf.parse("08-06-2013"));
	        dto.setCreatedBy("Admin");
            dto.setModifiedBy("Admin");
            dto.setCreatedDatetime(new Timestamp(new Date().getTime()));
            dto.setModifiedDatetime(new Timestamp(new Date().getTime()));
           long pk = model.add(dto); 
            System.out.println("Successfully add"); 
          
            TimetableDTO addedDto = model.findByPK(pk); 
            if (addedDto == null) { 
                System.out.println("Test add fail"); 
            } 
        } catch (ApplicationException e) { 
            e.printStackTrace(); 
        } catch (DuplicateRecordException e) { 
            e.printStackTrace(); 
        }catch (HibernateException e) {
		e.printStackTrace();	// TODO: handle exception
		}
 
    } 
*/ 
    /** 
     * Tests delete a User 
     */ 
    public static void testDelete() { 
 
        try { 
            TimetableDTO dto = new TimetableDTO(); 
            long pk = 2L; 
            dto.setId(pk); 
            model.delete(dto); 
           TimetableDTO deletedDto = model.findByPK(pk); 
            if (deletedDto != null) { 
                System.out.println("Test Delete fail"); 
            } 
        } catch (ApplicationException e) { 
            e.printStackTrace(); 
        } 
    } 
    /** 
     * Tests update a User 
     */ 
    public static void testUpdate() throws ParseException { 
 
        try { 
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy"); 
            TimetableDTO dto = model.findByPK(1);
            dto.setCource_Name("bba");;;
            dto.setCreatedBy("deepak");
                      model.update(dto); 
 
            TimetableDTO updatedDTO = model 
                    .findByPK(1); 
            if ("".equals(updatedDTO.getId())) { 
                System.out.println("Test Update fail"); 
            } 
            else {
				System.out.println("update successfully");
			}
        } catch (ApplicationException e) { 
            e.printStackTrace(); 
        } catch (DuplicateRecordException e) { 
            e.printStackTrace(); 
        } 
    } 
   /* public static void testFindByLogin() throws ApplicationException { 
        TimetableDTO dto = new TimetableDTO(); 
         SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy"); 
		 
		 dto = model.checkByCourseName(dto.getCource_Id(),dto.getExam_Date());
		 if (dto == null) { 
		     System.out.println("Test Find By PK fail"); 
		 } 
		 System.out.println(dto.getCource_Name()); 
		 System.out.println(dto.getExam_time()); 
		 System.out.println(dto.getExam_Date()); 
		 System.out.println(dto.getCreatedBy()); 
		 System.out.println(dto.getModifiedBy()); 
     } 
    */
   public static void testFindByLogin1() throws ApplicationException, ParseException { 
        TimetableDTO dto = new TimetableDTO(); 
         SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy"); 
		 
		 dto = model.checkByExamTime(1L, 1L, "4th", sdf.parse("20-09-2019"), "10:00 am to 1:00 pm");
		 if (dto != null) { 
		     System.out.println("Test success"); 
		     
		 } 
		 else {
			 System.out.println("test fail");
		 }
		 
     } 
}
