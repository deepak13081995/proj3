package in.co.sunrays.proj3.util;

import java.text.ParseException; 
import java.text.SimpleDateFormat; 
import java.util.Date; 
 
/** 
 * This class validates input data 
 *  
 * @author SUNRAYS Technologies 
 * @version 1.0 
 * @Copyright (c) SUNRAYS Technologies 
 */ 
 
public class DataValidator { 
 
    /** 
     * Checks if value is Null 
     *  
     * @param val 
     * @return 
     */ 
    public static boolean isNull(String val) { 
        if (val == null || val.trim().length() == 0) { 
            return true; 
        } else { 
            return false; 
        } 
    } 
    
    /**
     * Checks if value is password
     * 
     * @param val
     * @return
     */


    public static boolean isPassword(String pass) { // my method created

    	String passreg = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,20})";
    	String spass = pass.trim();
    	int checkIndex = spass.indexOf(" ");

    	if (isNotNull(spass) && spass.matches(passreg) && checkIndex == -1) {
    		return true;
    	}

    	else {
    		return false;
    	}
    }

    
    /**
     * Checks if value is roll number
     * 
     * @param val
     * @return
     */

    public static boolean isRollNo(String roll) { // my method created
    	String rollreg = "^((?=.*\\d).{1,4}(?=.*[A-Z]).{1,2}(?=.*\\d).{1,6})$";
    	String sroll = roll.trim();

    	if (isNotNull(sroll)) {

    		boolean check = sroll.matches(rollreg);
    		//System.out.println(check);
    		return check;
    	}

    	else {

    		return false;
    	}
    }

 
    /** 
     * Checks if value is NOT Null 
     *  
     * @param val 
     * @return 
     */ 
    public static boolean isNotNull(String val) { 
        return !isNull(val); 
    } 
 
    /** 
     * Checks if value is an Integer 
     *  
     * @param val 
     * @return 
     */ 
 
    public static boolean isInteger(String val) { 
 
        if (isNotNull(val)) { 
            try { 
                int i = Integer.parseInt(val); 
                return true; 
            } catch (NumberFormatException e) { 
                return false; 
            } 
 
        } else { 
            return false; 
        } 
    } 
 
    /** 
     * Checks if value is Long 
     *  
     * @param val 
     * @return 
     */ 
    public static boolean isLong(String val) { 
        if (isNotNull(val)) { 
            try { 
                long i = Long.parseLong(val); 
                return true; 
            } catch (NumberFormatException e) { 
                return false; 
            } 
 
        } else { 
            return false; 
        } 
    } 
 
    /** 
     * Checks if value is valid Email ID 
     *  
     * @param val 
     * @return 
     */ 
    public static boolean isEmail(String val) { 
 
        String emailreg = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"; 
 
        if (isNotNull(val)) { 
            try { 
                return val.matches(emailreg); 
            } catch (NumberFormatException e) { 
                return false; 
            } 
 
        } else { 
            return false; 
        } 
    } 
 
    /** 
     * Checks if value is Date 
     *  
     * @param val 
     * @return 
     */ 
    public static boolean isDate(String val) { 
        Date d = null; 
        if (isNotNull(val)) { 
            d = DataUtility.getDate(val); 
        } 
        return d != null; 
    } 
    
    public static boolean isMobileNum(String mob){
    	String mobilereg="^[6-9][0-9]{9,12}$";
    	if(isNotNull(mob)&& mob.matches(mobilereg)){
    		return true;
    	}
    	else{
    		return false;
    	}
    }
    
    
    public static boolean isAddress(String name) { // my method created

    	String namereg = "^[0-9a-zA-Z\\s,-]+$";
    	//String namer ="^[a-zA-Z_-]+$";


    	String sname = name.trim();

    	if (isNotNull(sname) && sname.matches(namereg)) {

    		return true;
    	} else {
    		return false;
    	}
    }

    public static boolean isName1(String name) { // my method created

    	String namereg = "^[^-\\s][\\p{L} .']+$";
    	//String namer ="^[a-zA-Z_-]+$";

    	String sname = name.trim();

    	if (isNotNull(sname) && sname.matches(namereg)) {

    		return true;
    	} else {
    		return false;
    	}
    }


    
    /**
     * Checks if value is name
     * 
     * @param val
     * @return
     */


    public static boolean isName(String name) { // my method created

    	String namereg = "^[a-zA-Z_-]+$";
    	//String namer ="^[a-zA-Z_-]+$";


    	String sname = name.trim();

    	if (isNotNull(sname) && sname.matches(namereg)) {

    		return true;
    	} else {
    		return false;
    	}
    }

 
     
    /** 
     * Test above methods 
     *  
     * @param args 
     */ 
    public static void main(String[] args) { 
 
        System.out.println(isEmail("ankur.ertr232gmail.com")); 
       /* System.out.println(isDate("ewrwrwer")); 
        System.out.println(isDate("")); 
        System.out.println(isDate(null)); 
        System.out.println(DataUtility.getDate("24/29/2015")); 
 */
       // System.out.println(isMobileNum("1098989898"));
    } 
 
} 

