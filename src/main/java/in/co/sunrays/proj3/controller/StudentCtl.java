package in.co.sunrays.proj3.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import in.co.sunrays.proj3.dto.BaseDTO;
import in.co.sunrays.proj3.dto.StudentDTO;
import in.co.sunrays.proj3.exception.ApplicationException;
import in.co.sunrays.proj3.exception.DuplicateRecordException;
import in.co.sunrays.proj3.model.CollegeModelInt;
import in.co.sunrays.proj3.model.ModelFactory;
import in.co.sunrays.proj3.model.StudentModelInt;
import in.co.sunrays.proj3.util.DataUtility;
import in.co.sunrays.proj3.util.DataValidator;
import in.co.sunrays.proj3.util.PropertyReader;
import in.co.sunrays.proj3.util.ServletUtility;
 
/**
 * Student functionality Controller. Performs operation for add, update,
 * delete and get Student
 *  
 * @author SUNRAYS Technologies
 * @version 1.0
 * @Copyright (c) SUNRAYS Technologies
 */
@ WebServlet(name="StudentCtl",urlPatterns={"/ctl/StudentCtl"}) 
public class StudentCtl extends BaseCtl {
 
    private static Logger log = Logger.getLogger(StudentCtl.class);
 
    @Override
    protected void preload(HttpServletRequest request) {
        CollegeModelInt model = ModelFactory.getInstance().getCollegeModel();
        try {
            List l = model.list();
            request.setAttribute("collegeList", l);
        } catch (ApplicationException e) {
            log.error(e);
        }
 
    }
 
    @Override
    protected boolean validate(HttpServletRequest request) {
 
    	log.debug("StudentCtl Method validate Started");
		System.out.println("validate start");
		boolean pass=true;
		
		
		String op=DataUtility.getString(request.getParameter("operation"));
		
			
		if(DataValidator.isNull(request.getParameter("fname"))){
			
			request.setAttribute("firstName1",PropertyReader.getValue("error.require","FirstName"));
			
			pass=false;
			
		}
		
		else if(!DataValidator.isName(request.getParameter("fname"))){
			
			request.setAttribute("firstName1",PropertyReader.getValue("error.name","Invalid"));
			
			pass=false;
			
		}
		
		
		if(DataValidator.isNull(request.getParameter("lname"))){
			
			request.setAttribute("lastName",PropertyReader.getValue("error.require","LastName"));
			
			pass=false;
		}
		
		else if(!DataValidator.isName(request.getParameter("lname"))){
			
			request.setAttribute("lastName",PropertyReader.getValue("error.name","Invalid "));
			
			pass=false;
		}
		
		
		if(DataValidator.isNull(request.getParameter("mobile"))){
			
			request.setAttribute("mobileNo", PropertyReader.getValue("error.require","Mobile No"));
			
			pass=false;
			
		}
		

		else if(!DataValidator.isMobileNum(request.getParameter("mobile"))){
			
			request.setAttribute("mobileNo", PropertyReader.getValue("error.mobile"," Invalid Mobile"));
			
			pass=false;
			
		}
		
		
		
		if(DataValidator.isNull(request.getParameter("login"))){
			
			request.setAttribute("login",PropertyReader.getValue("error.require","Email"));
			
			pass=false;
			
		}
		else if(!DataValidator.isEmail(request.getParameter("login"))){
			
			request.setAttribute("login",PropertyReader.getValue("error.email","Invalid "));
			
			pass=false;
		}
		
		if(DataValidator.isNull(request.getParameter("collegeId"))){
			
			request.setAttribute("collegeId",PropertyReader.getValue("error.require","College Name"));
			
			pass=false;	
			
		}
		
		if(DataValidator.isNull(request.getParameter("dob"))){
			
			request.setAttribute("dob",PropertyReader.getValue("error.require","Dath Of Birth"));
			
			pass=false;
			
		}
	
		
		        log.debug("StudentCtl Method validate Ended");

        System.out.println("validate end");
        return pass;
    }
 
    @Override
    protected BaseDTO populateDTO(HttpServletRequest request) {
 
    	log.debug("StudentCtl Method populatebean Started");
		System.out.println("popu start");
		StudentDTO dto=new StudentDTO();
		
		dto.setId(DataUtility.getLong(request.getParameter("id")));
		
		dto.setFirstName(DataUtility.getString(request.getParameter("fname")));
		
		dto.setLastName(DataUtility.getString(request.getParameter("lname")));
		
		dto.setDob(DataUtility.getDate(request.getParameter("dob")));
		
		//dto.setAddress(DataUtility.getString(request.getParameter("address")));
		
		dto.setMobileNo(DataUtility.getString(request.getParameter("mobile")));
		
		dto.setEmail(DataUtility.getString(request.getParameter("login")));
		
	    dto.setCollegeId(DataUtility.getLong(request.getParameter("collegeId")));
		
		dto.setCollegeName(DataUtility.getString(request.getParameter("collegeId")));
		
		populateDTO(dto, request);
		
		 log.debug("StudentCtl Method populatebean Ended");
		 System.out.println("validate end");
	        return dto;
    }
    /**
     * Contains Display logics
     */    
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   		
   		log.debug("SubjectCtl Method doGet Started");
		 System.out.println("get suru");
		 String op=DataUtility.getString(request.getParameter("operation"));
		 
		StudentModelInt model = ModelFactory.getInstance().getStudentModel();
		  
		 long id=DataUtility.getLong(request.getParameter("id"));
		 
		 if(id>0 || op!=null){
			 System.out.println("id>0");	 
			StudentDTO dto =new StudentDTO();
			 
			 try {
				dto=model.findByPK(id);
				ServletUtility.setDto(dto, request);
				
			} catch (ApplicationException e) {
		       
				 log.error(e);
	             ServletUtility.handleException(e, request, response);
	             return;

			}
		 }
		 System.out.println("get puri");
		 ServletUtility.forward(getView(), request, response);
	     log.debug("StudentCtl Method doGetEnded");
	     System.out.println("do get");


   		
		}



	/**
     * Contains Submit logics
     */  
    
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
 
        log.debug("StudentCtl Method doGet Started");
 
        String op = DataUtility.getString(request.getParameter("operation"));
 
        // get model
 
        StudentModelInt model = ModelFactory.getInstance().getStudentModel();
 
        long id = DataUtility.getLong(request.getParameter("id"));
        StudentDTO dto = (StudentDTO) populateDTO(request);
        
        if (OP_SAVE.equalsIgnoreCase(op)) {
 
           
            try {
                if (id > 0) {
                    model.update(dto);
                    ServletUtility.setDto(dto, request);
                    
                } else {
                    long pk = model.add(dto);
                    dto.setId(pk);
                }
 
                ServletUtility.setSuccessMessage("Student successfully added",
                        request);
 
            } catch (ApplicationException e) {
                log.error(e);
                ServletUtility.handleException(e, request, response);
                return;
            } catch (DuplicateRecordException e) {
                ServletUtility.setDto(dto, request);
                ServletUtility.setErrorMessage(
                        "Email Id already exists", request);
            }
 
        } else if(OP_UPDATE.equalsIgnoreCase(op)){
     	   
     	   try {
     		   if(id>0){
				model.update(dto);
				ServletUtility.setDto(dto, request);
	               
	               ServletUtility.setSuccessMessage("Student updated successfully ",request);
	               //ServletUtility.forward(getView(), request, response);
	               System.out.println("Save end");
     		   }
				
			} catch (ApplicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
     	   
     	   catch (DuplicateRecordException e) {
				// TODO Auto-generated catch block
     		   ServletUtility.setDto(dto, request);
     		   ServletUtility.setErrorMessage("Email id already exists", request);
     		   
				e.printStackTrace();
			}
     	   
     	   
     	   
        }
        else if (OP_RESET.equalsIgnoreCase(op)) {
	     	System.out.println("cancle Start");
	         ServletUtility.forward(getView(), request, response);
	     	System.out.println("cancle end");
	         return;
	         
	     }
     	   
        else if (OP_CANCEL.equalsIgnoreCase(op)) {
 
            ServletUtility
                    .redirect(ORSView.STUDENT_LIST_CTL, request, response);
            return;
 
        } 
        ServletUtility.forward(ORSView.STUDENT_VIEW, request, response);
 
        log.debug("StudentCtl Method doGet Ended");
    }
 
    @Override
    protected String getView() {
        return ORSView.STUDENT_VIEW;
    }
 
}
