package in.co.sunrays.proj3.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import in.co.sunrays.proj3.dto.BaseDTO;
import in.co.sunrays.proj3.dto.RoleDTO;
import in.co.sunrays.proj3.exception.ApplicationException;
import in.co.sunrays.proj3.exception.DuplicateRecordException;
import in.co.sunrays.proj3.model.ModelFactory;
import in.co.sunrays.proj3.model.RoleModelInt;
import in.co.sunrays.proj3.util.DataUtility;
import in.co.sunrays.proj3.util.DataValidator;
import in.co.sunrays.proj3.util.PropertyReader;
import in.co.sunrays.proj3.util.ServletUtility;
 
/**
 * Role functionality Controller. Performs operation for add, update and
 * get Role
 *  
 * @author SUNRAYS Technologies
 * @version 1.0
 * @Copyright (c) SUNRAYS Technologies
 */
 
@ WebServlet(name="RoleCtl",urlPatterns={"/ctl/RoleCtl"})

public class RoleCtl extends BaseCtl {
 
    private static final long serialVersionUID = 1L;
 
    private static Logger log = Logger.getLogger(RoleCtl.class);
 
    @Override
    protected boolean validate(HttpServletRequest request) {
 
log.debug("RoleCtl Method validate Started");
		
		boolean pass=true;
		
		if(DataValidator.isNull(request.getParameter("name"))){
			
			
			request.setAttribute("name",PropertyReader.getValue("error.require","Name"));
			
			pass=false;
		}
		
		else if(!DataValidator.isName1(request.getParameter("name"))){
			
			
			request.setAttribute("name",PropertyReader.getValue("error.role","Invalid "));
			
			pass=false;
		}
		
		
		
		
		if(DataValidator.isNull(request.getParameter("description"))){
			
			request.setAttribute("description",PropertyReader.getValue("error.require","Description"));
			
			pass=false;
		}
		else if(!DataValidator.isName1(request.getParameter("description"))){
			
			request.setAttribute("description",PropertyReader.getValue("error.description","Invalid"));
			
			pass=false;
		}
		
		
		 log.debug("RoleCtl Method validate Ended");
               System.out.println("validate");
	        return pass;
    }
 
    @Override
    protected BaseDTO populateDTO(HttpServletRequest request) {
 
 log.debug("RoleCtl Method populatebean Started");
		 
		 RoleDTO dto=new RoleDTO();
		 
		 dto.setId(DataUtility.getLong(request.getParameter("id")));
		 //System.out.println("id  "+bean.getId());
		 
		 dto.setName(DataUtility.getString(request.getParameter("name")));
		  //System.out.println("name  "+bean.getName());
		  
		 dto.setDescription(DataUtility.getString(request.getParameter("description")));
		 //System.out.println("desc  "+bean.getDescription());
		 
		 populateDTO(dto,request);
		 
		  log.debug("RoleCtl Method populatebean Ended");
		  System.out.println("populate Baean");
	        return dto;
    }
 
    /**
     * Contains Display logics
     */    
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   		
   		log.debug("RoleCtl Method doGet Started");
		 System.out.println("get suru");
		 String op=DataUtility.getString(request.getParameter("operation"));
		 
		RoleModelInt model = ModelFactory.getInstance().getRoleModel();
		  
		 long id=DataUtility.getLong(request.getParameter("id"));
		 
		 if(id>0 || op!=null){
			 System.out.println("id>0");	 
			RoleDTO dto =new RoleDTO();
			 
			 try {
				dto=model.findByPK(id);
				ServletUtility.setDto(dto, request);
				
			} catch (ApplicationException e) {
		       
				 log.error(e);
	             ServletUtility.handleException(e, request, response);
	             return;

			}
		 }
		 System.out.println("get puri");
		 ServletUtility.forward(getView(), request, response);
	     log.debug("RoleCtl Method doGetEnded");
	     System.out.println("do get");


   		
		}

    
    
    /**
     * Contains Submit logics
     */    
protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        log.debug("RoleCtl Method doGet Started");
 
        System.out.println("In do get");
 
        String op = DataUtility.getString(request.getParameter("operation"));
 
        // get model
        RoleModelInt model = ModelFactory.getInstance().getRoleModel();
        RoleDTO dto = (RoleDTO) populateDTO(request);
        
 
        long id = DataUtility.getLong(request.getParameter("id"));
        
        if (OP_SAVE.equalsIgnoreCase(op)) {
 
           
            try {
                if (id > 0) {
                    model.update(dto);
                } else {
                    long pk = model.add(dto);
                    dto.setId(pk);
                }
 
               // ServletUtility.setDto(dto, request);
                ServletUtility.setSuccessMessage("Role successfully saved",
                        request);
 
            } catch (ApplicationException e) {
                log.error(e);
                ServletUtility.handleException(e, request, response);
                return;
            } catch (DuplicateRecordException e) {
                ServletUtility.setDto(dto, request);
                ServletUtility.setErrorMessage("Role already exists", request);
            }
 
        
        }
        
        else if(OP_UPDATE.equalsIgnoreCase(op)){
     	   
     	   try {
     		   if(id>0){
				model.update(dto);
				ServletUtility.setDto(dto, request);
	               
	               ServletUtility.setSuccessMessage("Role successfully updated",request);
	               //ServletUtility.forward(getView(), request, response);
	               System.out.println("Save end");
     		   }
				
			} catch (ApplicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
     	   
     	   catch (DuplicateRecordException e) {
				// TODO Auto-generated catch block
     		   ServletUtility.setDto(dto, request);
     		   ServletUtility.setErrorMessage("Role already exists", request);
     		   
				e.printStackTrace();
			}
     	   
     	   
     	   
        }
     	   
         else if (OP_CANCEL.equalsIgnoreCase(op)) {
 
            ServletUtility.redirect(ORSView.ROLE_LIST_CTL, request, response);
            return;
 
        } 
        ServletUtility.forward(ORSView.ROLE_VIEW, request, response);
 
        log.debug("RoleCtl Method doGet Ended");
    }
 
    @Override
    protected String getView() {
        return ORSView.ROLE_VIEW;
    }
 
}
