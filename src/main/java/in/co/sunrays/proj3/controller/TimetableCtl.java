package in.co.sunrays.proj3.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import in.co.sunrays.proj3.dto.BaseDTO;
import in.co.sunrays.proj3.dto.TimetableDTO;
import in.co.sunrays.proj3.exception.ApplicationException;
import in.co.sunrays.proj3.exception.DuplicateRecordException;
import in.co.sunrays.proj3.model.CourseModelInt;
import in.co.sunrays.proj3.model.ModelFactory;
import in.co.sunrays.proj3.model.SubjectModelInt;
import in.co.sunrays.proj3.model.TimetableModelInt;
import in.co.sunrays.proj3.util.DataUtility;
import in.co.sunrays.proj3.util.DataValidator;
import in.co.sunrays.proj3.util.PropertyReader;
import in.co.sunrays.proj3.util.ServletUtility;
 
/**
 * TimetableCtl functionality Controller. Performs operation for add, update,
 * delete and get College
 *  
 * @author SUNRAYS Technologies
 * @version 1.0
 * @Copyright (c) SUNRAYS Technologies
 */
@ WebServlet(name="TimetableCtl",urlPatterns={"/ctl/TimetableCtl"})
public class TimetableCtl extends BaseCtl {
	 
    private static final long serialVersionUID = 1L;
 
    private static Logger log = Logger.getLogger(TimetableCtl.class);
    
    
    protected void preload(HttpServletRequest request){
    	CourseModelInt model = ModelFactory.getInstance().getCourseModel();
		 
    	SubjectModelInt model1=ModelFactory.getInstance().getSubjectModel();
    	
    	try {
    		List l=model.list();
    		List l1=model1.list();
    		
    		 request.setAttribute("courceName",l);
    		 request.setAttribute("subjectName",l1);
    		System.out.println("preload");
    	} catch (ApplicationException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    		log.error(e);
    	}
    }

 
    @Override
    protected boolean validate(HttpServletRequest request) {
 
    	log.debug("TimetableCtl Method validate Started");
		
		boolean pass=true;
		
		if(DataValidator.isNull(request.getParameter("courceId"))){
			
			
			request.setAttribute("courceId",PropertyReader.getValue("error.require","Cource Name"));
	 		
			pass=false;
		}
		
        if(DataValidator.isNull(request.getParameter("subjectId"))){
			
			
			request.setAttribute("subjectId",PropertyReader.getValue("error.require","Subject Name"));
			
			pass=false;
		}
        
         if(DataValidator.isNull(request.getParameter("semester"))){
			
			
			request.setAttribute("semester",PropertyReader.getValue("error.require","Semester Name"));
			
			pass=false;
		}
        
        
       if(DataValidator.isNull(request.getParameter("examtime"))){
	
	
	    request.setAttribute("examtime",PropertyReader.getValue("error.require","Exam Time"));
	
	        pass=false;
}


         if(DataValidator.isNull(request.getParameter("examdate"))){
	
	
	      request.setAttribute("examdate",PropertyReader.getValue("error.require","Exam Date"));
	
	        pass=false;
}


			 log.debug("TimetableCtl Method validate Ended");
               System.out.println("validate  "+pass);
	        return pass;
   }
 
    @Override
    protected BaseDTO populateDTO(HttpServletRequest request) {
 
        log.debug("TimetableCtl Method populateDTO Started");
 
        TimetableDTO dto = new TimetableDTO();
       
    	
		 
		 dto.setId(DataUtility.getLong(request.getParameter("id")));
		 //System.out.println("id  "+bean.getId());
		 
		 dto.setCource_Id(DataUtility.getInt(request.getParameter("courceId")));
		 //System.out.println("course id"+(request.getParameter("courceId")));
		
		 dto.setSubject_Id(DataUtility.getInt(request.getParameter("subjectId")));
		 
		dto.setCource_Name(DataUtility.getString(request.getParameter("courceId")));
		  //System.out.println("name  "+bean.getCource_Name());
		  
		 dto.setSubject_Name(DataUtility.getString(request.getParameter("subjectId")));
		 //System.out.println("duration  "+bean.getSubject_Name());
		 
		 dto.setSemester(DataUtility.getString(request.getParameter("semester")));
		 
		dto.setExam_time(DataUtility.getString(request.getParameter("examtime")));
		 
		 dto.setExam_Date(DataUtility.getDate(request.getParameter("examdate")));
		
		 log.debug("TimetableCtl Method populateDTO Ended"); 
	        
	        populateDTO(dto, request);
	 
	        return dto; 
   }

    

	/**
     * Contains Display logics
     */    
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   		
   		log.debug("TimetableCtl Method doGet Started");
		 System.out.println("get suru");
		 String op=DataUtility.getString(request.getParameter("operation"));
		 
		 TimetableModelInt model = ModelFactory.getInstance().getTimetableModel();
		  
		 long id=DataUtility.getLong(request.getParameter("id"));
		 
		 if(id>0 || op!=null){
			 System.out.println("id>0");	 
			 TimetableDTO dto =new TimetableDTO();
			 
			 try {
				dto=model.findByPK(id);
				ServletUtility.setDto(dto, request);
				
			} catch (ApplicationException e) {
		       
				 log.error(e);
	             ServletUtility.handleException(e, request, response);
	             return;

			}
		 }
		 System.out.println("get puri");
		 ServletUtility.forward(getView(), request, response);
	     log.debug("timetableCtl Method doGetEnded");
	     System.out.println("do get");


   		
		}

    
    
	/**
     * Contains Display logics
     */    
 protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
 
        log.debug("TimetableCtl Method doGet Started");
 
        String op = DataUtility.getString(request.getParameter("operation"));
 
        // get model
        TimetableModelInt model = ModelFactory.getInstance().getTimetableModel();
 
        long id = DataUtility.getLong(request.getParameter("id"));
        TimetableDTO dto = (TimetableDTO)populateDTO(request);
        
		 if (OP_SAVE.equalsIgnoreCase(op)) {
			 System.out.println("save Start");
	            try {
	               
	                   //model.update(bean);
	                
	               	//System.out.println("if me gaya");
	                   long pk;
					try {
						pk = model.add(dto);
						// bean.setId(pk);
						 ServletUtility.setSuccessMessage("timetable successfully added",request);
						// ServletUtility.setDto(dto, request);
			               System.out.println("Save end");
					
					} catch (DuplicateRecordException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
			
						ServletUtility.setErrorMessage("Timetable already exists", request);
						ServletUtility.setDto(dto, request);
					}
	                  
	              //ServletUtility.setBean(bean, request);
	               
	              
	           } catch (ApplicationException e) {
	           	e.printStackTrace();
	               log.error(e);
	               
	               ServletUtility.handleException(e, request, response);
	               
	               return;
	           }
	            }
	           
	           else if(OP_UPDATE.equalsIgnoreCase(op)){
	        	   
	        	   try {
	        		   if(id>0){
					model.update(dto);
					ServletUtility.setDto(dto, request);
		               
		               ServletUtility.setSuccessMessage("timetable successfully updated",request);
		               //ServletUtility.forward(getView(), request, response);
		               System.out.println("Save end");
	        		   }
					
				} catch (ApplicationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
	        	   
	        	   catch (DuplicateRecordException e) {
					// TODO Auto-generated catch block
	        		   ServletUtility.setDto(dto, request);
	        		   ServletUtility.setErrorMessage("Timetable already exists", request);
	        		   
					e.printStackTrace();
				}
	        	   
	        	   
	        	   
	           }
	        	   
	           
		  else if (OP_RESET.equalsIgnoreCase(op)) {
	     	System.out.println("cancle Start");
	         ServletUtility.forward(getView(), request, response);
	     	System.out.println("cancle end");
	         return;
	         
	     }else if (OP_CANCEL.equalsIgnoreCase(op)) {

	            ServletUtility.redirect(ORSView.TIMETABLE_LIST_CTL, request,
	                    response);
	            return;

	        }

		 ServletUtility.forward(getView(), request, response);
		 
		 log.debug("CourceCtl Method doPOst Ended");

    }
 
    @Override
    protected String getView() {
        return ORSView.TIMETABLE_VIEW;
    }
 
}
