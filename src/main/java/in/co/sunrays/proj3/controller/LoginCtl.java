package in.co.sunrays.proj3.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import in.co.sunrays.proj3.dto.BaseDTO;
import in.co.sunrays.proj3.dto.RoleDTO;

/**
 * Servlet implementation class LoginCtl
 */

import in.co.sunrays.proj3.dto.UserDTO;
import in.co.sunrays.proj3.exception.ApplicationException;
import in.co.sunrays.proj3.model.ModelFactory;
import in.co.sunrays.proj3.model.RoleModelInt;
import in.co.sunrays.proj3.model.UserModelInt;
import in.co.sunrays.proj3.util.DataUtility;
import in.co.sunrays.proj3.util.DataValidator;
import in.co.sunrays.proj3.util.PropertyReader;
import in.co.sunrays.proj3.util.ServletUtility; 
 
/** 
 * Login functionality Controller. Performs operation for Login 
 *  
 *  
 * @author SUNRAYS Technologies 
 * @version 1.0 
 * @Copyright (c) SUNRAYS Technologies 
 */ 
@WebServlet(name="LoginCtl",urlPatterns={"/LoginCtl"}) 
public class LoginCtl extends BaseCtl { 
 
    private static final long serialVersionUID = 1L; 
    public static final String OP_RESET = "Reset"; 
    public static final String OP_SIGN_IN = "SignIn"; 
    public static final String OP_SIGN_UP = "SignUp"; 
    public static final String OP_LOG_OUT = "logout"; 
    private static Logger log = Logger.getLogger(LoginCtl.class); 
 
    @Override 
    protected boolean validate(HttpServletRequest request) { 
 
        log.debug("LoginCtl Method validate Started"); 
 
        boolean pass = true; 
 
        String op = request.getParameter("operation"); 
        if (OP_SIGN_UP.equals(op) || OP_LOG_OUT.equals(op)) { 
            return pass; 
        } 
 
        String login = request.getParameter("login"); 
 
        if (DataValidator.isNull(login)) { 
            request.setAttribute("login", 
                    PropertyReader.getValue("error.require", "Login Id")); 
            pass = false; 
        } else if (!DataValidator.isEmail(login)) { 
            request.setAttribute("login", 
                    PropertyReader.getValue("error.email","")); 
            pass = false; 
        } 
        if (DataValidator.isNull(request.getParameter("password"))) { 
            request.setAttribute("password", 
                    PropertyReader.getValue("error.require", "Password")); 
            pass = false; 
        } 
        
        else if (!DataValidator.isPassword(request.getParameter("password"))) { 
            request.setAttribute("password", 
                    PropertyReader.getValue("error.password", "Invalid")); 
            pass = false; 
        } 
 
 
        log.debug("LoginCtl Method validate Ended"); 
 
        return pass; 
    } 
 
    @Override 
    protected BaseDTO populateDTO(HttpServletRequest request) { 
 
        log.debug("LoginCtl Method populateDTO Started"); 
 
        UserDTO dto = new UserDTO(); 
 
        dto.setId(DataUtility.getLong(request.getParameter("id"))); 
        dto.setLogin(DataUtility.getString(request.getParameter("login"))); 
        System.out.println(request.getParameter("login"));
        dto.setPassword(DataUtility.getString(request.getParameter("password"))); 
        System.out.println(request.getParameter("password"));
        
 
        log.debug("LoginCtl Method populateDTO Ended"); 
        populateDTO(dto, request);
        return dto; 
    } 
    
    /**
     * Display Login form
     * 
     */
   
    
    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        
        log.debug(" Method doGet Started");

        String op = DataUtility.getString(request.getParameter("operation"));

        // get model
        /*UserModel model = new UserModel();
        RoleModel role = new RoleModel();

        long id = DataUtility.getLong(request.getParameter("id"));
        if (id > 0) {
            UserBean userbean;
            try {
                userbean = model.findByPK(id);
                System.out.println(userbean.getFirstName());
                ServletUtility.setBean(userbean, request);
            } catch (ApplicationException e) {
                log.error(e);
                ServletUtility.handleException(e, request, response);
                return;
            }
        }
*/        if (OP_LOG_OUT.equals(op)) {
	
	        HttpSession session = request.getSession(true);
            session = request.getSession();
            ServletUtility.setSuccessMessage("Logout successfully!!!!!", request);
            session.invalidate();
            
           

            ServletUtility.forward(getView(), request, response);
            
            return;
        }

        ServletUtility.forward(getView(), request, response);

        log.debug("UserCtl Method doPost Ended");

    }
 

    
    /**
     * Submitting or login action performing
     * 
     */
   
    protected void doPost(HttpServletRequest request, 
            HttpServletResponse response) throws ServletException, IOException { 
        HttpSession session = request.getSession(true); 
        log.debug(" Method doGet Started"); 
 
        String op = DataUtility.getString(request.getParameter("operation")); 
 
        // get model 
        UserModelInt model = ModelFactory.getInstance().getUserModel(); 
        RoleModelInt role = ModelFactory.getInstance().getRoleModel(); 
 
        long id = DataUtility.getLong(request.getParameter("id")); 

        if (OP_SIGN_IN.equalsIgnoreCase(op)) { 
       
            UserDTO dto = (UserDTO) populateDTO(request);
          
          
            try { 
 
                dto = model.authenticate(dto.getLogin(), dto.getPassword()); 
               
                
                if (dto != null) { 
                    session.setAttribute("user", dto); 
                    long rollId = dto.getRoleId(); 
 
                    RoleDTO roleDTO = role.findByPK(rollId); 
 
                    if (roleDTO != null) { 
                        session.setAttribute("role", roleDTO.getName());
                      
                    }
                    
                    String str=(String)session.getAttribute("URI");
                    
                    if(str==null || "null".equalsIgnoreCase(str)){
                    ServletUtility.redirect(ORSView.WELCOME_CTL, request, response);
                     return;
                     
                    }
                    
                    else{
                    	
                    	if(rollId==1 ||rollId==2 ||rollId==3 || rollId==4 || rollId==5){
                    		ServletUtility.redirect(str, request, response);
                    		
                    	}
                    	else{
                    		
                      ServletUtility.redirect(ORSView.WELCOME_CTL, request, response);
                    	}return;
                    }
                
 
                   } else { 
                    dto = (UserDTO) populateDTO(request); 
                    ServletUtility.setDto(dto, request); 
                    ServletUtility.setErrorMessage( 
                            "Invalid LoginId And Password", request); 
                } 
 
            } catch (ApplicationException e) { 
                log.error(e); 
                ServletUtility.handleException(e, request, response); 
                return; 
            } 
 
        }  else if (OP_SIGN_UP.equalsIgnoreCase(op)) { 
 
            ServletUtility.redirect(ORSView.USER_REGISTRATION_CTL, request, 
                    response); 
            return; 
 
        } 
 
        else { // View page 
 
            if (id > 0 || op != null) { 
                UserDTO userDTO; 
                try { 
                    userDTO = model.findByPK(id); 
                    ServletUtility.setDto(userDTO, request); 
                } catch (ApplicationException e) { 
                    log.error(e); 
                    ServletUtility.handleException(e, request, response); 
                    return; 
                } 
            } 
        } 
 
        ServletUtility.forward(getView(), request, response); 
 
        log.debug("UserCtl Method doGet Ended"); 
    } 
 
    @Override 
    protected String getView() { 
        return ORSView.LOGIN_VIEW; 
    } 
 
}
