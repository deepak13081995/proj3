package in.co.sunrays.proj3.model;





import in.co.sunrays.proj3.dto.CollegeDTO;
import in.co.sunrays.proj3.dto.CourseDTO;
import in.co.sunrays.proj3.dto.SubjectDTO;
import in.co.sunrays.proj3.dto.TimetableDTO;
import in.co.sunrays.proj3.exception.ApplicationException;
import in.co.sunrays.proj3.exception.DatabaseException;
import in.co.sunrays.proj3.exception.DuplicateRecordException;
import in.co.sunrays.proj3.util.HibDataSource;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;


/**
 * Hibernate Implementation of CollegeModel
 *
 * @author SUNRAYS Technologies
 * @version 1.0
 * @Copyright (c) SUNRAYS Technologies
 */

public class TimetableModelHibImpl implements TimetableModelInt {

	 private static Logger log = Logger.getLogger(TimetableModelHibImpl.class);

	    /**
	     * Add a College
	     *
	     * @param dto
	     * @throws DatabaseException
	     *
	     */
public long add(TimetableDTO dto) throws ApplicationException, DuplicateRecordException {
	log.debug("Model add Started");
    long pk = 0;
    
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy"); 
	 
	 TimetableDTO duplicatetimetable = checkByExamTime(dto.getCource_Id(),dto.getSubject_Id(),dto.getSemester(),dto.getExam_Date(),dto.getExam_time());
		 if (duplicatetimetable != null) {
		        throw new DuplicateRecordException("Timetable already exists");
		    }
	
      CourseModelInt model = ModelFactory.getInstance().getCourseModel();
    CourseDTO cbean=model.findByPK(dto.getCource_Id());
    dto.setCource_Name(cbean.getName());
  
    SubjectModelInt model2=ModelFactory.getInstance().getSubjectModel();
    SubjectDTO sbean=model2.findByPK(dto.getSubject_Id());
   dto.setSubject_Name(sbean.getSubjectName());        

    
    
    Session session = HibDataSource.getSession();
    Transaction transaction = null;
    try {
        transaction = session.beginTransaction();
        session.save(dto);
        pk = dto.getId();
        transaction.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
        log.error("Database Exception..", e);
        if (transaction != null) {
            transaction.rollback();
        }
        throw new ApplicationException("Exception in College Add "
                + e.getMessage());
    } finally {
        session.close();
    }

    log.debug("Model add End");
    return dto.getId();	}

/**
 * Update a Collage
 *
 * @param dto
 * @throws DatabaseException
 */
public void update(TimetableDTO dto) throws ApplicationException, DuplicateRecordException {
	 log.debug("Model update Started");
     Session session = null;
     Transaction transaction = null;

        TimetableDTO duplicatetimetable = checkByExamTime(dto.getCource_Id(),dto.getSubject_Id(),dto.getSemester(),dto.getExam_Date(),dto.getExam_time());
	 if (duplicatetimetable != null) {
	        throw new DuplicateRecordException("Timetable already exists");
	    }


     CourseModelInt model = ModelFactory.getInstance().getCourseModel();
     CourseDTO cbean=model.findByPK(dto.getCource_Id());
     dto.setCource_Name(cbean.getName());
   
     SubjectModelInt model2=ModelFactory.getInstance().getSubjectModel();
     SubjectDTO sbean=model2.findByPK(dto.getSubject_Id());
    dto.setSubject_Name(sbean.getSubjectName());        


     try {

         session = HibDataSource.getSession();
         transaction = session.beginTransaction();
         session.update(dto);
         transaction.commit();
     } catch (HibernateException e) {
         log.error("Database Exception..", e);
         if (transaction != null) {
             transaction.rollback();
             throw new ApplicationException("Exception in College Update"
                     + e.getMessage());
         }
     } finally {
         session.close();
     }
     log.debug("Model update End");	
	}

/**
 * Delete a College
 *
 * @param dto
 * @throws DatabaseException
 */
public void delete(TimetableDTO dto) throws ApplicationException {
	 log.debug("Model delete Started");
     Session session = null;
     Transaction transaction = null;
     try {
         session = HibDataSource.getSession();
         transaction = session.beginTransaction();
         session.delete(dto);
         transaction.commit();
     } catch (HibernateException e) {
         log.error("Database Exception..", e);
         if (transaction != null) {
             transaction.rollback();
         }
         throw new ApplicationException("Exception in college Delete"
                 + e.getMessage());
     } finally {
         session.close();
     }
     log.debug("Model delete End");	}

	public TimetableDTO findByExamDate(Date date) throws ApplicationException {
		log.debug("Model findByName Started");
        Session session = null;
        TimetableDTO dto = null;
        try {
            session = HibDataSource.getSession();
            Criteria criteria = session.createCriteria(CollegeDTO.class);
            criteria.add(Restrictions.eq("exam_Date", date));
            List list = criteria.list();
            if (list.size() > 0) {
                dto = (TimetableDTO) list.get(0);
            }

        } catch (HibernateException e) {
            log.error("Database Exception..", e);
            throw new ApplicationException(
                    "Exception in getting User by Login " + e.getMessage());

        } finally {
            session.close();
        }

        log.debug("Model findByName End");
        return dto;
	}


    /**
     * Find Collage by PK
     *
     * @param pk
     *            : get parameter
     * @return dto
     * @throws DatabaseException
     */
public TimetableDTO findByPK(long pk) throws ApplicationException {
	 log.debug("Model findByPK Started");
     Session session = null;
     TimetableDTO dto = null;
     try {
         session = HibDataSource.getSession();
         dto = (TimetableDTO) session.get(TimetableDTO.class, pk);
     } catch (HibernateException e) {
         log.error("Database Exception..", e);
         throw new ApplicationException(
                 "Exception : Exception in getting College by pk");
     } finally {
         session.close();
     }

     log.debug("Model findByPK End");
     return dto;
	}


/**
 * Searches Colleges with pagination
 *
 * @return list : List of Colleges
 * @param dto
 *            : Search Parameters
 * @param pageNo
 *            : Current Page No.
 * @param pageSize
 *            : Size of Page
 *
 * @throws DatabaseException
 */

	public List search(TimetableDTO dto, int pageNo, int pageSize) throws ApplicationException {
		
		
		log.debug("Model search Started");
        Session session = null;
        List list = null;
        try {
            session = HibDataSource.getSession();
            Criteria criteria = session.createCriteria(TimetableDTO.class);
            
            if (dto!=null) {
            	  if (dto.getId() > 0) {
                      criteria.add(Restrictions.eq("id", dto.getId()));
                  }
                  
                  if(dto.getCource_Name()!=null && dto.getCource_Name().length()>0) {
                      criteria.add(Restrictions.like("cource_Name", dto.getCource_Name() + "%"));
                 
                  }
                  if(dto.getSubject_Name()!=null && dto.getSubject_Name().length()>0) {
                      criteria.add(Restrictions.like("subject_Name", dto.getSubject_Name() + "%"));
                 
                  }
                  if(dto.getCource_Id()>0) {
                      criteria.add(Restrictions.eq("cource_Id", dto.getCource_Id()));
                 
                  }
                  
                  if(dto.getSubject_Id()>0) {
                      criteria.add(Restrictions.eq("subject_Id", dto.getSubject_Id()));
                 
                  }
                 if (dto.getExam_Date() != null) {
                	System.out.println("============>>"+dto.getExam_Date());
                //	  SimpleDateFormat sm=new SimpleDateFormat("dd-MM-yyyy");
  	            //	Date d=new Date(dto.getExam_Date().getTime());
  	            	criteria.add(Restrictions.eq("exam_Date",dto.getExam_Date()));
  	                
  	            }
  	       
                 
            	}
            
            
            // if page size is greater than zero the apply pagination
            if (pageSize > 0) {
                criteria.setFirstResult(((pageNo - 1) * pageSize));
                criteria.setMaxResults(pageSize);
            }

            list = criteria.list();
        } catch (HibernateException e) {
        	e.printStackTrace();
            log.error("Database Exception..", e);
            throw new ApplicationException("Exception in college search");
        } finally {
            session.close();
        }

        log.debug("Model search End");
        return list;	}

	
	
	 /**
     * Search College
     *
     * @param dto
     *            : Search Parameters
     * @throws DatabaseException
     */
  public List search(TimetableDTO dto) throws ApplicationException {
		// TODO Auto-generated method stub
	  return search(dto, 0, 0);
	}

	
  
  /**
   * Gets List of College
   *
   * @return list : List of College
   * @throws DatabaseException
   */
 public List list() throws ApplicationException {
		// TODO Auto-generated method stub
	 return list(0, 0);
	}

	
	 /**
     * get List of College with pagination
     *
     * @return list : List of College
     * @param pageNo
     *            : Current Page No.
     * @param pageSize
     *            : Size of Page
     * @throws DatabaseException
     */
   public List list(int pageNo, int pageSize) throws ApplicationException {
	   log.debug("Model list Started");
       Session session = null;
       List list = null;
       try {
           session = HibDataSource.getSession();
           Criteria criteria = session.createCriteria(TimetableDTO.class);

           // if page size is greater than zero then apply pagination
           if (pageSize > 0) {
               pageNo = ((pageNo - 1) * pageSize) + 1;
               criteria.setFirstResult(pageNo);
               criteria.setMaxResults(pageSize);
           }

           list = criteria.list();
       } catch (HibernateException e) {
           log.error("Database Exception..", e);
           throw new ApplicationException(
                   "Exception : Exception in  College list");
       } finally {
           session.close();
       }

       log.debug("Model list End");
       return list;
	}
  
	public TimetableDTO findByCSD(String course, String semester, Date date) {
		log.debug("Model findByName Started");
	       Session session = null;
	       TimetableDTO dto = null;
	       
	       Date date1 = new Date(date.getTime());
	       try {
	           session = HibDataSource.getSession();
	           Criteria criteria = session.createCriteria(TimetableDTO.class);
	           criteria.add(Restrictions.like("cource_Name",course)).add(Restrictions.like("semester",semester)).add(Restrictions.like("exam_Date",date1));
	          // criteria.add(Restrictions.eq("semester",semester)); 
	           //criteria.add(Restrictions.eq("exam_Date",date));
	           List list = criteria.list();
	           if (list.size() > 0) {
	               dto = (TimetableDTO) list.get(0);
	           }

	       } catch (HibernateException e) {
	           log.error("Database Exception..", e);
	           e.printStackTrace();
	           try {
				throw new ApplicationException(
				           "Exception in getting User by Login " + e.getMessage());
				
			} catch (ApplicationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

	       } finally {
	           session.close();
	       }

	       log.debug("Model CSD End");
	       return dto;

	}

	public TimetableDTO findBySCS(String subject, String course, String semester) {
		log.debug("Model findByName Started");
        Session session = null;
        TimetableDTO dto = null;
        try {
            session = HibDataSource.getSession();
            Criteria criteria = session.createCriteria(CollegeDTO.class);
            criteria.add(Restrictions.eq("subject_Name",subject));
            criteria.add(Restrictions.eq("cource_Name",course)); 
            criteria.add(Restrictions.eq("semester",semester));
            List list = criteria.list();
            if (list.size() > 0) {
                dto = (TimetableDTO) list.get(0);
            }

        } catch (HibernateException e) {
            log.error("Database Exception..", e);
            try {
				throw new ApplicationException(
				        "Exception in getting User by Login " + e.getMessage());
			} catch (ApplicationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

        } finally {
            session.close();
        }

        log.debug("Model CSD End");
        return dto;

	}
	public TimetableDTO checkByExamTime(long courseId, long subjectId, String semester, Date examDate, String examTime){
		log.debug("model checkbyexamtime start");
		Session session = null;
		TimetableDTO dto = null;
			session = HibDataSource.getSession();
			
			Date date1 = new Date(examDate.getTime());
			Query q = session.createQuery("from TimetableDTO where cource_Id=? and subject_Id=? and semester=? and exam_Date=? and exam_Time=?");
			q.setLong(0, courseId);
			q.setLong(1, subjectId);
			q.setString(2, semester);
			q.setDate(3, date1);
			q.setString(4, examTime);
			 List list = q.list();
		        if (list.size() > 0) {
		            dto = (TimetableDTO) list.get(0);
		        } else {
		            dto = null;
		     }
		        log.debug("model exam time end");
		        return dto;
	}
	
	/**checkByCourseName
	 * @param courseId
	 * @param examDate
	 * @return
	 * @throws ApplicationException
	 */
	public TimetableDTO checkByCourseName1(int courseId, Date examDate){
		log.debug(" model checkByCourseName start");
		Session session = null;
		TimetableDTO dto = null;
		
		session = HibDataSource.getSession();
		Query q = session.createQuery("from TimetableDTO where courseId=? and examDate=?");
		q.setLong(0, courseId);
		q.setDate(1, examDate);
		List list = q.list();
		 if (list.size() > 0) {
	            dto = (TimetableDTO) list.get(0);
	        } else {
	            dto = null;
	     }
	        log.debug("model exam time end");
	        return dto;
	}
	
	/**
	 * @param courseId
	 * @param subjectId
	 * @param examDate
	 * @return
	 * @throws ApplicationException
	 */
	public TimetableDTO checkBySubjectName(Long courseId, Long subjectId, Date examDate){
	log.debug("model check by subject name start");
	Session session = null;
	TimetableDTO dto = null;
	session = HibDataSource.getSession();
	Query q = session.createQuery("from TimetableDTO where courseId=? and subjectId=? and examDate=?");
	q.setLong(0, courseId);
	q.setLong(1, subjectId);
	q.setDate(2, examDate);
	List list = q.list();
	 if (list.size() > 0) {
           dto = (TimetableDTO) list.get(0);
       } else {
           dto = null;
    }
       log.debug("model checksubject end");
       return dto;
 }
	
	/**
	 * @param courseId
	 * @param subjectId
	 * @param semester
	 * @param examDate
	 * @return
	 * @throws ApplicationException
	 */
	
	public TimetableDTO checkBySemester(Long courseId, Long subjectId, String semester, Date examDate){
		log.debug("model checkBySemester start");
		Session session = null;
		TimetableDTO dto = null;
		session = HibDataSource.getSession();
		Query q = session.createQuery("from TimetableDTO where courseId=? and subjectId=? and semester=? and examDate=?");
		q.setLong(0, courseId);
		q.setLong(1, subjectId);
		q.setString(2, semester);
		q.setDate(3, examDate);
		List list = q.list();
		 if (list.size() > 0) {
	           dto = (TimetableDTO) list.get(0);
	       } else {
	           dto = null;
	    }
	       log.debug("model checksubject end");
	       return dto;
	}

	public TimetableDTO checkByCourseName(int cource_Id, java.util.Date exam_Date) {
		log.debug(" model checkByCourseName start");
		Session session = null;
		TimetableDTO dto = null;
		
		session = HibDataSource.getSession();
		Query q = session.createQuery("from TimetableDTO where COURSE_ID=? and EXAM_DATE=?");
		q.setLong(0, cource_Id);
		q.setDate(1, exam_Date);
		List list = q.list();
		 if (list.size() > 0) {
	            dto = (TimetableDTO) list.get(0);
	        } else {
	            dto = null;
	     }
	        log.debug("model exam time end");
	        return dto;	}


	
}
