package in.co.sunrays.proj3.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import org.apache.log4j.Logger;

import in.co.sunrays.proj3.dto.CourseDTO;
import in.co.sunrays.proj3.dto.FacultyDTO;
import in.co.sunrays.proj3.dto.SubjectDTO;
import in.co.sunrays.proj3.dto.TimetableDTO;
import in.co.sunrays.proj3.exception.ApplicationException;
import in.co.sunrays.proj3.exception.DatabaseException;
import in.co.sunrays.proj3.exception.DuplicateRecordException;
import in.co.sunrays.proj3.util.JDBCDataSource;

public class TimetableModelJDBCImpl implements TimetableModelInt{
	
	
	 private static Logger log = Logger.getLogger(TimetableModelJDBCImpl.class);

	    /**
	     * Find next PK of TImetable
	     * 
	     * @throws DatabaseException
	     */
	    public Integer nextPK() throws DatabaseException {
	        log.debug("Model nextPK Started");
	        Connection conn = null;
	        int pk = 0;
	        try {
	            conn = JDBCDataSource.getConnection();
	            PreparedStatement pstmt = conn
	                    .prepareStatement("SELECT MAX(ID) FROM ST_TIMETABLE");
	            ResultSet rs = pstmt.executeQuery();
	            while (rs.next()) {
	                pk = rs.getInt(1);
	            }
	            rs.close();

	        } catch (Exception e) {
	            log.error("Database Exception..", e);
	            throw new DatabaseException("Exception : Exception in getting PK");
	        } finally {
	            JDBCDataSource.closeConnection(conn);
	        }
	        log.debug("Model nextPK End");
	        return pk + 1;
	    }

	
	
	    /**
	     * Add a Faculty
	     * 
	     * @param bean
	     * @throws DatabaseException
	     * 
	     */
	   
	public long add(TimetableDTO dto) throws ApplicationException, DuplicateRecordException {
		log.debug("Model add Started");
        Connection conn = null;

        // get College Name
                int pk = 0;
        CourseModelJDBCImpl model=new CourseModelJDBCImpl();
        CourseDTO cbean=model.findByPK(dto.getCource_Id());
        SubjectModelJDBCImpl model1=new SubjectModelJDBCImpl();
        SubjectDTO sbean=model1.findByPK(dto.getSubject_Id());//subject name and cource name yaha se set hoge
        
        dto.setCource_Name(cbean.getName());
        dto.setSubject_Name(sbean.getSubjectName());
        
        System.out.println("Daaaaaaaaa"+dto.getExam_Date());
        
        TimetableModelJDBCImpl m=new TimetableModelJDBCImpl();
        TimetableDTO b=m.findBySCS(dto.getSubject_Name(),dto.getCource_Name(),dto.getSemester());
        TimetableDTO b1=m.findBySCD(dto.getSubject_Name(),dto.getCource_Name(),new java.sql.Date(dto.getExam_Date().getTime())) ;
        
        if(b!=null || b1!=null){
        	throw new DuplicateRecordException("time table exists");
        	
        }
        
        try {
            conn = JDBCDataSource.getConnection();
            pk = nextPK();
            // Get auto-generated next primary key
            System.out.println(pk + " in ModelJDBC");
            conn.setAutoCommit(false); // Begin transaction
            PreparedStatement pstmt = conn
                    .prepareStatement("INSERT INTO ST_TIMETABLE VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
            pstmt.setLong(1,pk);
            pstmt.setInt(2, dto.getCource_Id());
            pstmt.setString(3, dto.getCource_Name());
            pstmt.setInt(4, dto.getSubject_Id());
            pstmt.setString(5, dto.getSubject_Name());
            pstmt.setString(6,dto.getSemester());
            pstmt.setString(7, dto.getExam_time());
            pstmt.setDate(8,new java.sql.Date(dto.getExam_Date().getTime()));
            pstmt.setString(9,dto.getCreatedBy());
            pstmt.setString(10,dto.getModifiedBy());
            pstmt.setTimestamp(11,dto.getCreatedDatetime());
            pstmt.setTimestamp(12, dto.getModifiedDatetime());
            pstmt.executeUpdate();
            
            conn.commit(); // End transaction
            pstmt.close();
           
        } catch (Exception e) {
            log.error("Database Exception..", e);
            try {
                conn.rollback();
            } catch (Exception ex) {
                throw new ApplicationException(
                        "Exception : add rollback exception " + ex.getMessage());
            }
            e.printStackTrace();
            throw new ApplicationException(
                    "Exception : Exception in add Timetable");
        } finally {
            JDBCDataSource.closeConnection(conn);
        }
        log.debug("Model add End");
        return pk;
	}
	
	 public TimetableDTO findBySCD(String courceName,String SEMESTER,Date date) throws ApplicationException {
	        log.debug("Model findBy Email Started");
	        StringBuffer sql = new StringBuffer(
	                "SELECT * FROM ST_TIMETABLE WHERE SUBJECT_NAME=? AND SEMESTER=? AND EXAM_DATE=?");
	        TimetableDTO dto = null;
	        Connection conn = null;
	       // Date d = new Date(date.getTime());
	        try {
	            conn = JDBCDataSource.getConnection();
	            PreparedStatement pstmt = conn.prepareStatement(sql.toString());
	            pstmt.setString(1, courceName);
	            pstmt.setString(2, SEMESTER);
	            pstmt.setDate(3,(java.sql.Date)date);
	            ResultSet rs = pstmt.executeQuery();
	            while (rs.next()) {
	                dto = new TimetableDTO();
	                dto.setId(rs.getLong(1));
	                dto.setCource_Id(rs.getInt(2));
	                dto.setCource_Name(rs.getString(3));
	                dto.setSubject_Id(rs.getInt(4));
	                dto.setSubject_Name(rs.getString(5));
	                dto.setSemester(rs.getString(6));
	                dto.setExam_time(rs.getString(7));
	                dto.setExam_Date(rs.getDate(8));
	                dto.setCreatedBy(rs.getString(9));
	                dto.setModifiedBy(rs.getString(10));;
	                dto.setCreatedDatetime(rs.getTimestamp(11));
	                dto.setModifiedDatetime(rs.getTimestamp(12));

	            }
	            rs.close();
	        } catch (Exception e) {
	        	e.printStackTrace();
	            log.error("Database Exception..", e);
	            throw new ApplicationException(
	                    "Exception : Exception in getting User by subject name");
	        } finally {
	            JDBCDataSource.closeConnection(conn);
	        }
	        log.debug("Model findBy subjectName End");
	        return dto;
	    }
	    
	/*    public TimetableDTO findBySCS(String subjectName,String courceName,String sem) throws ApplicationException {
	        log.debug("Model findBy Email Started");
	        StringBuffer sql = new StringBuffer(
	                "SELECT * FROM ST_TIMETABLE WHERE SUBJECT_NAME=? AND COURCE_NAME=? AND SEMESTER=?");
	        TimetableDTO dto = null;
	        Connection conn = null;
	      
	        try {
	            conn = JDBCDataSource.getConnection();
	            PreparedStatement pstmt = conn.prepareStatement(sql.toString());
	            pstmt.setString(1, subjectName);
	            pstmt.setString(2, courceName);
	            pstmt.setString(3,sem);
	            ResultSet rs = pstmt.executeQuery();
	            while (rs.next()) {
	                dto = new TimetableDTO();
	                dto.setId(rs.getLong(1));
	                dto.setCource_Id(rs.getInt(2));
	                dto.setCource_Name(rs.getString(3));
	                dto.setSubject_Id(rs.getInt(4));
	                dto.setSubject_Name(rs.getString(5));
	                dto.setSemester(rs.getString(6));
	                dto.setExam_time(rs.getString(7));
	                dto.setExam_Date(rs.getDate(8));
	                dto.setCreatedBy(rs.getString(9));
	                dto.setModifiedBy(rs.getString(10));;
	                dto.setCreatedDatetime(rs.getTimestamp(11));
	                dto.setModifiedDatetime(rs.getTimestamp(12));

	            }
	            rs.close();
	        } catch (Exception e) {
	            log.error("Database Exception..", e);
	            throw new ApplicationException(
	                    "Exception : Exception in getting User by subject name");
	        } finally {
	            JDBCDataSource.closeConnection(conn);
	        }
	        log.debug("Model findBy subjectName End");
	        return dto;
	    }
*/	    

	    
	    /**
	     * Update a Student
	     * 
	     * @param bean
	     * @throws DatabaseException
	     */
	public void update(TimetableDTO dto) throws ApplicationException, DuplicateRecordException {
		// TODO Auto-generated method stub
		  log.debug("Model update Started");
	        
	        Connection conn = null;
	        CourseModelJDBCImpl model=new CourseModelJDBCImpl();
          CourseDTO cbean=model.findByPK(dto.getCource_Id());
          SubjectModelJDBCImpl model1=new SubjectModelJDBCImpl();
          SubjectDTO sbean=model1.findByPK(dto.getSubject_Id());//subject name and cource name yaha se set hoge
	        
          dto.setCource_Name(cbean.getName());
          dto.setSubject_Name(sbean.getSubjectName());
	       
          TimetableModelJDBCImpl m=new TimetableModelJDBCImpl();
          TimetableDTO b=m.findBySCS(dto.getSubject_Name(),dto.getCource_Name(),dto.getSemester());
          TimetableDTO b1=m.findBySCD(dto.getSubject_Name(),dto.getCource_Name(),new java.sql.Date(dto.getExam_Date().getTime())) ;
          
          if(b!=null || b1!=null){
          	throw new DuplicateRecordException("time table exists");
          	
          }
         
          
	        
	        try {

	            conn = JDBCDataSource.getConnection();

	            conn.setAutoCommit(false); // Begin transaction
	            PreparedStatement pstmt = conn
	                    .prepareStatement("UPDATE ST_TIMETABLE SET COURCE_ID=?,COURCE_NAME=?,SUBJECT_ID=?,SUBJECT_NAME=?,SEMESTER=?,EXAM_TIME=?,"
	                + "EXAM_DATE=?,CREATED_BY=?,MODIFIED_BY=?,CREATED_DATETIME=?,MODIFIED_DATETIME=? WHERE ID=?");
	            
	           
	            pstmt.setInt(1,dto.getCource_Id());
	            pstmt.setString(2,dto.getCource_Name());
	            pstmt.setInt(3, dto.getSubject_Id());
	            pstmt.setString(4,dto.getSubject_Name());
	            pstmt.setString(5,dto.getSemester());
	            pstmt.setString(6,dto.getExam_time());
	            pstmt.setDate(7,new java.sql.Date(dto.getExam_Date().getTime()));
	            pstmt.setString(8,dto.getCreatedBy());
	            pstmt.setString(9,dto.getModifiedBy());
	            pstmt.setTimestamp(10,dto.getCreatedDatetime());
	            pstmt.setTimestamp(11,dto.getModifiedDatetime());
	            pstmt.setLong(12,dto.getId());
	            pstmt.executeUpdate();
	         
	            
	            pstmt.executeUpdate();
	            conn.commit(); // End transaction
	            pstmt.close();
	        } catch (Exception e) {
	        	e.printStackTrace();
	            log.error("Database Exception..", e);
	           /* try {
	                conn.rollback();
	            } catch (Exception ex) {
	                throw new ApplicationException(
	                        "Exception : Delete rollback exception "
	                                + ex.getMessage());
	            }
	            throw new ApplicationException("Exception in updating Student ");
	        */} finally {
	            JDBCDataSource.closeConnection(conn);
	        }
	        log.debug("Model update End");
	}

	
	 /**
     * Delete a Student
     * 
     * @param bean
     * @throws DatabaseException
     */
  
	public void delete(TimetableDTO dto) throws ApplicationException {
		// TODO Auto-generated method stub
		 log.debug("Model delete Started");
	        Connection conn = null;
	        try {
	            conn = JDBCDataSource.getConnection();
	            conn.setAutoCommit(false); // Begin transaction
	            PreparedStatement pstmt = conn
	                    .prepareStatement("DELETE FROM ST_TIMETABLE WHERE ID=?");
	            pstmt.setLong(1,dto.getId());
	           
	            pstmt.executeUpdate();
	           
	            conn.commit(); // End transaction
	           
	            pstmt.close();
	           

	        } catch (Exception e) {
	            log.error("Database Exception..", e);
	            try {
	                conn.rollback();
	            } catch (Exception ex) {
	                throw new ApplicationException(
	                        "Exception : Delete rollback exception "
	                                + ex.getMessage());
	            }
	            throw new ApplicationException(
	                    "Exception : Exception in delete Timetable");
	        } finally {
	            JDBCDataSource.closeConnection(conn);
	        }
	        log.debug("Model delete Started");	
	}
	
	  /**
     * Find User by Student
     * 
     * @param Email
     *            : get parameter
     * @return bean
     * @throws DatabaseException
     */

    public TimetableDTO findByCourceName(String courceName) throws ApplicationException {
        log.debug("Model findBy Email Started");
        StringBuffer sql = new StringBuffer(
                "SELECT * FROM ST_TIMETABLE WHERE COURCE_NAME=?");
        TimetableDTO dto = null;
        Connection conn = null;
        try {
            conn = JDBCDataSource.getConnection();
            PreparedStatement pstmt = conn.prepareStatement(sql.toString());
            pstmt.setString(1, courceName);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                dto = new TimetableDTO();
                dto.setId(rs.getLong(1));
                dto.setCource_Id(rs.getInt(2));
                dto.setCource_Name(rs.getString(3));
                dto.setSubject_Id(rs.getInt(4));
                dto.setSubject_Name(rs.getString(5));
                dto.setSemester(rs.getString(6));
                dto.setExam_time(rs.getString(7));
                dto.setExam_Date(rs.getDate(8));
                dto.setCreatedBy(rs.getString(9));
                dto.setModifiedBy(rs.getString(10));;
                dto.setCreatedDatetime(rs.getTimestamp(11));
                dto.setModifiedDatetime(rs.getTimestamp(12));

            }
            rs.close();
        } catch (Exception e) {
            log.error("Database Exception..", e);
            throw new ApplicationException(
                    "Exception : Exception in getting User by Email");
        } finally {
            JDBCDataSource.closeConnection(conn);
        }
        log.debug("Model findBy courceName End");
        return dto;
    }


	public TimetableDTO findByEmailId(String emailId) throws ApplicationException {
		// TODO Auto-generated method stub
		return null;
	}

	
	 /**
     * Find Student by PK
     * 
     * @param pk
     *            : get parameter
     * @return bean
     * @throws DatabaseException
     */

	public TimetableDTO findByPK(long pk) throws ApplicationException {
		 log.debug("Model findByPK Started");
	        StringBuffer sql = new StringBuffer("SELECT * FROM ST_TIMETABLE WHERE ID=?");
	        TimetableDTO dto = null;
	        Connection conn = null;
	        try {
	            conn = JDBCDataSource.getConnection();
	            PreparedStatement pstmt = conn.prepareStatement(sql.toString());
	            pstmt.setLong(1, pk);
	            ResultSet rs = pstmt.executeQuery();
	            while (rs.next()) {
	            	 dto = new TimetableDTO();
	            	 dto.setId(rs.getLong(1));
		                dto.setCource_Id(rs.getInt(2));
		                dto.setCource_Name(rs.getString(3));
		                dto.setSubject_Id(rs.getInt(4));
		                dto.setSubject_Name(rs.getString(5));
		                dto.setSemester(rs.getString(6));
		                dto.setExam_time(rs.getString(7));
		                dto.setExam_Date(rs.getDate(8));
		                dto.setCreatedBy(rs.getString(9));
		                dto.setModifiedBy(rs.getString(10));;
		                dto.setCreatedDatetime(rs.getTimestamp(11));
		                dto.setModifiedDatetime(rs.getTimestamp(12));

	               }
	            rs.close();
	        } catch (Exception e) {
	            log.error("Database Exception..", e);
	            throw new ApplicationException(
	                    "Exception : Exception in getting Faculty by pk");
	        } finally {
	            JDBCDataSource.closeConnection(conn);
	        }
	        log.debug("Model findByPK End");
	        return dto;
	}

	
	 /**
     * Search Student with pagination
     * 
     * @return list : List of Students
     * @param bean
     *            : Search Parameters
     * @param pageNo
     *            : Current Page No.
     * @param pageSize
     *            : Size of Page
     * 
     * @throws DatabaseException
     */

	public List search(TimetableDTO dto, int pageNo, int pageSize) throws ApplicationException {
		 log.debug("Model search Started");
	        StringBuffer sql = new StringBuffer("SELECT * FROM ST_TIMETABLE WHERE 1=1");
	System.out.println("search start");
	        if (dto != null) {
	            /*if (bean.getId() > 0) {
	                sql.append(" AND id = " + bean.getId());
	            }*/
	            if (dto.getCource_Id()>0) {
	                sql.append(" AND COURCE_ID = " +dto.getCource_Id());
	                }  
	            
/*	            if (bean.getCource_Name() != null && bean.getCource_Name().length() > 0) {
	                sql.append(" AND COURCE_NAME like '" + bean.getCource_Name() + "'");
	            }*/
	            
	            if (dto.getSubject_Id() > 0) {
	                sql.append(" AND SUBJECT_ID = " +dto.getSubject_Id());
	            }
	          
	          /*  if (bean.getSubject_Name()!= null && bean.getSubject_Name().length() > 0) {
	                sql.append(" AND SUBJECT_NAME like '" + bean.getSubject_Name()
	                        + "%'");
	            }
	            
	            if (bean.getSemester() != null && bean.getSemester().length()>0) {
	                sql.append(" AND SEMESTER like '" + bean.getSemester()
	                        + "%'");
	            }
	          
	            if (bean.getExam_time() != null) {
	                sql.append(" AND EXAM_TIME like '" + bean.getExam_time()
	                        + "%'");
	            }
	            */
	            if (dto.getExam_Date() != null) {
	            	Date d=new Date(dto.getExam_Date().getTime());
	                sql.append(" AND EXAM_DATE like '" + d + "%'");   
	            }
	         
	            System.out.println("search end");
	                   }

	        // if page size is greater than zero then apply pagination
	        if (pageSize > 0) {
	            // Calculate start record index
	            pageNo = (pageNo - 1) * pageSize;

	            sql.append(" Limit " + pageNo + ", " + pageSize);
	            // sql.append(" limit " + pageNo + "," + pageSize);
	        }
	        System.out.println("final sql   "+sql);
	        List list = new ArrayList();
	        Connection conn = null;
	        System.out.println("connection par");
	        try {
	            conn = JDBCDataSource.getConnection();
	            PreparedStatement pstmt = conn.prepareStatement(sql.toString());
	            System.out.println("preap par");
	            ResultSet rs = pstmt.executeQuery();
	            System.out.println("resultset par");
	           
	            while (rs.next()) {
	                TimetableDTO dto1 = new TimetableDTO();
	                System.out.println("rs.next" );
	                dto1.setId(rs.getLong(1));
	                dto1.setCource_Id(rs.getInt(2));
	                dto1.setCource_Name(rs.getString(3));
	                dto1.setSubject_Id(rs.getInt(4));
	                dto1.setSubject_Name(rs.getString(5));
	                dto1.setSemester(rs.getString(6));
	                dto1.setExam_time(rs.getString(7));
	                dto1.setExam_Date(rs.getDate(8));
	                dto1.setCreatedBy(rs.getString(9));
	                dto1.setModifiedBy(rs.getString(10));;
	                dto1.setCreatedDatetime(rs.getTimestamp(11));
	                dto1.setModifiedDatetime(rs.getTimestamp(12));
	                System.out.println("add "+rs.getTimestamp(11) );
	              list.add(dto1);
	            }
	            rs.close();
	        } catch (Exception e) {
	        	//e.printStackTrace();
	            log.error("Database Exception..", e);
	            throw new ApplicationException(
	                    "Exception : Exception in search timetable");
	        } finally {
	            JDBCDataSource.closeConnection(conn);
	        }

	        log.debug("Model search End");
	        return list;
	}

	
	 /**
     * Search Student
     * 
     * @param bean
     *            : Search Parameters
     * @throws DatabaseException
     */
	 public List search(TimetableDTO dto) throws ApplicationException {
		// TODO Auto-generated method stub
		  return search(dto, 0, 0);
	}

	 
	 
	 /**
	     * Get List of faculty
	     * 
	     * @return list : List of Student
	     * @throws DatabaseException
	     */

	public List list() throws ApplicationException {
		// TODO Auto-generated method stub
		 return list(0, 0);
	}

	
	/**
     * Get List of Student with pagination
     * 
     * @return list : List of Student
     * @param pageNo
     *            : Current Page No.
     * @param pageSize
     *            : Size of Page
     * @throws DatabaseException
     */
	public List list(int pageNo, int pageSize) throws ApplicationException {
		 log.debug("Model list Started");
	        List list = new ArrayList();
	        StringBuffer sql = new StringBuffer("select * from ST_TIMETABLE");
	        // if page size is greater than zero then apply pagination
	        if (pageSize > 0) {
	            // Calculate start record index
	            pageNo = (pageNo - 1) * pageSize;
	            sql.append(" limit " + pageNo + "," + pageSize);
	        }
	      
	        Connection conn = null;

	        try {
	            conn = JDBCDataSource.getConnection();
	            PreparedStatement pstmt = conn.prepareStatement(sql.toString());
	            ResultSet rs = pstmt.executeQuery();
	            while (rs.next()) {
	               TimetableDTO dto=new TimetableDTO();
	                
	               dto.setId(rs.getLong(1));
	                dto.setCource_Id(rs.getInt(2));
	                dto.setCource_Name(rs.getString(3));
	                dto.setSubject_Id(rs.getInt(4));
	                dto.setSubject_Name(rs.getString(5));
	                dto.setSemester(rs.getString(6));
	                dto.setExam_time(rs.getString(7));
	                dto.setExam_Date(rs.getDate(8));
	                dto.setCreatedBy(rs.getString(9));
	                dto.setModifiedBy(rs.getString(10));;
	                dto.setCreatedDatetime(rs.getTimestamp(11));
	                dto.setModifiedDatetime(rs.getTimestamp(12));

	                               list.add(dto);
	            }
	            rs.close();
	        } catch (Exception e) {
	            log.error("Database Exception..", e);
	            throw new ApplicationException(
	                    "Exception : Exception in getting list of TIMETABLE");
	        } finally {
	            JDBCDataSource.closeConnection(conn);
	        }

	        log.debug("Model list End");
	        return list;

	}



	public TimetableDTO findByExamDate(Date emailId) throws ApplicationException {
		// TODO Auto-generated method stub
		return null;
	}



	public TimetableDTO findByCSD(String course, String semester, Date date) {
		// TODO Auto-generated method stub
		return null;
	}



	public TimetableDTO findBySCS(String subject, String course, String semester) {
		// TODO Auto-generated method stub
		return null;
	}



	public TimetableDTO checkByExamTime(Long courseId, Long subjectId, String semester, Date examDate,
			String examTime) {
		// TODO Auto-generated method stub
		return null;
	}



	public TimetableDTO checkBySubjectName(Long courseId, Long subjectId, Date examDate) {
		// TODO Auto-generated method stub
		return null;
	}



	public TimetableDTO checkBySemester(Long courseId, Long subjectId, String semester, Date examDate) {
		// TODO Auto-generated method stub
		return null;
	}



	public TimetableDTO checkByCourseName(int cource_Id, java.util.Date exam_Date) {
		// TODO Auto-generated method stub
		return null;
	}



	public TimetableDTO findByExamDate(java.util.Date emailId) throws ApplicationException {
		// TODO Auto-generated method stub
		return null;
	}



	public TimetableDTO findByCSD(String course, String semester, java.util.Date date) {
		// TODO Auto-generated method stub
		return null;
	}



	public TimetableDTO checkByExamTime(long courseId, long subjectId, String semester, java.util.Date examDate,
			String examTime) {
		// TODO Auto-generated method stub
		return null;
	}



	public TimetableDTO checkBySubjectName(Long courseId, Long subjectId, java.util.Date examDate) {
		// TODO Auto-generated method stub
		return null;
	}



	public TimetableDTO checkBySemester(Long courseId, Long subjectId, String semester, java.util.Date examDate) {
		// TODO Auto-generated method stub
		return null;
	}

}
