package in.co.sunrays.proj3.model;



import in.co.sunrays.proj3.dto.CourseDTO;
import in.co.sunrays.proj3.exception.ApplicationException;
import in.co.sunrays.proj3.exception.DatabaseException;
import in.co.sunrays.proj3.exception.DuplicateRecordException;

import java.util.List;

/**
 * Data Access Object of course
 *
 * @author SUNRAYS Technologies
 * @version 1.0
 * @Copyright (c) SUNRAYS Technologies
 */

public interface CourseModelInt {

    /**
     * Add a course
     *
     * @param dto
     * @throws ApplicationException
     * @throws DuplicateRecordException
     *             : throws when course already exists
     */
    public long add(CourseDTO dto) throws ApplicationException,
            DuplicateRecordException;

    /**
     * Update a course
     *
     * @param dto
     * @throws ApplicationException
     * @throws DuplicateRecordException
     *             : if updated course record is already exist
     */
    public void update(CourseDTO dto) throws ApplicationException,
            DuplicateRecordException;

    /**
     * Delete a course
     *
     * @param dto
     * @throws ApplicationException
     */
    public void delete(CourseDTO dto) throws ApplicationException;

    /**
     * Find course by email
     *
     * @param name
     *            : get parameter
     * @return dto
     * @throws ApplicationException
     */
    public CourseDTO  findByEmailId(String emailId) throws ApplicationException;

    public CourseDTO  findByName(String emailId) throws ApplicationException;

    /**
     * Find course by PK
     *
     * @param pk
     *            : get parameter
     * @return dto
     * @throws ApplicationException
     */
    public CourseDTO findByPK(long pk) throws ApplicationException;

    /**
     * Search course with pagination
     *
     * @return list : List of course
     * @param dto
     *            : Search Parameters
     * @param pageNo
     *            : Current Page No.
     * @param pageSize
     *            : Size of Page
     * @throws ApplicationException
     */
    public List search(CourseDTO dto, int pageNo, int pageSize)
            throws ApplicationException;

    /**
     * Search course
     *
     * @return list : List of course
     * @param dto
     *            : Search Parameters
     * @throws ApplicationException
     */
    public List search(CourseDTO dto) throws ApplicationException;

    /**
     * Gets List of course
     *
     * @return list : List of course
     * @throws DatabaseException
     */
    public List list() throws ApplicationException;
    /**
     * get List of course with pagination
     *
     * @return list : List of course
     * @param pageNo
     *            : Current Page No.
     * @param pageSize
     *            : Size of Page
     * @throws ApplicationException
     */
    public List list(int pageNo, int pageSize) throws ApplicationException;

}


	

