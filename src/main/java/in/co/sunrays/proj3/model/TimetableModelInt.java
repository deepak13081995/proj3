package in.co.sunrays.proj3.model;



import in.co.sunrays.proj3.dto.TimetableDTO;
import in.co.sunrays.proj3.exception.ApplicationException;
import in.co.sunrays.proj3.exception.DatabaseException;
import in.co.sunrays.proj3.exception.DuplicateRecordException;

import java.util.Date;
import java.util.List;

/**
 * Data Access Object of Timetable
 *
 * @author SUNRAYS Technologies
 * @version 1.0
 * @Copyright (c) SUNRAYS Technologies
 */

public interface TimetableModelInt {

    /**
     * Add a Timetable
     *
     * @param dto
     * @throws ApplicationException
     * @throws DuplicateRecordException
     *             : throws when Student already exists
     */
    public long add(TimetableDTO dto) throws ApplicationException,
            DuplicateRecordException;

    /**
     * Update a Timetable
     *
     * @param dto
     * @throws ApplicationException
     * @throws DuplicateRecordException
     *             : if updated user record is already exist
     */
    public void update(TimetableDTO dto) throws ApplicationException,
            DuplicateRecordException;

    /**
     * Delete a Timetable
   
     * @param dto
     * @throws ApplicationException
     */
    public void delete(TimetableDTO dto) throws ApplicationException;

    /**
     * Find Subject by Timetable
     *
     * @param name
     *            : get parameter
     * @return dto
     * @throws ApplicationException
     */
    public TimetableDTO findByExamDate(Date emailId) throws ApplicationException;

    /**
     * Find Timetable by PK
     *
     * @param pk
     *            : get parameter
     * @return dto
     * @throws ApplicationException
     */
    public TimetableDTO findByPK(long pk) throws ApplicationException;

    /**
     * Search Timetable with pagination
     *
     * @return list : List of Timetable
     * @param dto
     *            : Search Parameters
     * @param pageNo
     *            : Current Page No.
     * @param pageSize
     *            : Size of Page
     * @throws ApplicationException
     */
    public List search(TimetableDTO dto, int pageNo, int pageSize)
            throws ApplicationException;

    /**
     * Search Timetable
     *
     * @return list : List of Subject
     * @param dto
     *            : Search Parameters
     * @throws ApplicationException
     */
    public List search(TimetableDTO dto) throws ApplicationException;

    /**
     * Gets List of Timetable
     *
     * @return list : List of Timetable
     * @throws DatabaseException
     */
    public List list() throws ApplicationException;
    /**
     * get List of Timetable with pagination
     *
     * @return list : List of Timetable
     * @param pageNo
     *            : Current Page No.
     * @param pageSize
     *            : Size of Page
     * @throws ApplicationException
     */
    public List list(int pageNo, int pageSize) throws ApplicationException;
    public TimetableDTO findByCSD(String course,String semester,Date date);
    public TimetableDTO findBySCS(String subject,String course,String semester);
    public TimetableDTO checkByExamTime(long courseId, long subjectId, String semester, Date examDate, String examTime);
   // public TimetableDTO checkByCourseName(int courseId, Date examDate);
    public TimetableDTO checkBySubjectName(Long courseId, Long subjectId, Date examDate);
    public TimetableDTO checkBySemester(Long courseId, Long subjectId, String semester, Date examDate);

	public TimetableDTO checkByCourseName(int cource_Id, java.util.Date exam_Date);
    
}




	

